package com.altynbekova.bees_simulator.entity;

public class WorkerBee extends Bee {
    private double birthProbability;

    public WorkerBee() {
    }

    public WorkerBee(int birthPeriodicity, double birthProbability) {
        super(birthPeriodicity);
        this.birthProbability = birthProbability;
    }

    public double getBirthProbability() {
        return birthProbability;
    }

    public void setBirthProbability(double birthProbability) {
        this.birthProbability = birthProbability;
    }

    @Override
    public String toString() {
        return "WorkerBee";
    }
}
