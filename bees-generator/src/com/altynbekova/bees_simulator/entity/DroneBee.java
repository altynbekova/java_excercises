package com.altynbekova.bees_simulator.entity;

import java.sql.Timestamp;
import java.util.Random;

public class DroneBee extends Bee {
    private double maxQuantityCoefficient;

    public DroneBee() {
    }

    public DroneBee(int birthPeriodicity, double maxQuantityCoefficient) {
        super(birthPeriodicity);
        this.maxQuantityCoefficient = maxQuantityCoefficient;
    }

    @Override
    public String toString() {
        return "DroneBee";
    }
}
