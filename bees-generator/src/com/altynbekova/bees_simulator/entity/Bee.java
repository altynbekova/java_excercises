package com.altynbekova.bees_simulator.entity;

public class Bee {
    private int birthPeriodicity;

    public Bee() {
    }

    public Bee(int birthPeriodicity) {
        this.birthPeriodicity = birthPeriodicity;
    }

    public int getBirthPeriodicity() {
        return birthPeriodicity;
    }

    public void setBirthPeriodicity(int birthPeriodicity) {
        this.birthPeriodicity = birthPeriodicity;
    }
}
