package com.altynbekova.bees_simulator.service;

import com.altynbekova.bees_simulator.entity.Bee;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

public abstract class Habitat extends TimerTask implements IBehaviour{
    protected static List<Bee> bees=new ArrayList<>();
    protected int dronesAmount=0;
    protected int workersAmount=0;

    public static List<Bee> getBees() {
        return bees;
    }

    public int getDronesAmount(){
        return dronesAmount;
    }

    public int getWorkersAmount() {
        return workersAmount;
    }
}
