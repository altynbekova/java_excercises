package com.altynbekova.bees_simulator.service;

import com.altynbekova.bees_simulator.entity.DroneBee;

import java.util.Date;

public class DronesHabitat extends Habitat {
    private int droneBirthPeriodicity;
    private double droneBirthCoefficient;
    private double dronesPercentage;

    public DronesHabitat() {
    }

    public DronesHabitat(int droneBirthPeriodicity, double droneBirthCoefficient) {
        this.droneBirthPeriodicity = droneBirthPeriodicity;
        this.droneBirthCoefficient = droneBirthCoefficient;
    }

    @Override
    public DroneBee generateNewBee() {
        return new DroneBee(droneBirthPeriodicity,droneBirthCoefficient);
    }

    @Override
    public void run() {
        dronesPercentage=(bees.size()==0?0:(double)dronesAmount / bees.size());
        System.out.println("Drone bees percentage="+dronesPercentage);

        if (dronesPercentage < droneBirthCoefficient) {
            System.out.println(new Date());
            DroneBee bee = generateNewBee();
            System.out.println(bee + " generated\n");

            bees.add(bee);
            dronesAmount++;
        }
        else
            System.out.println(new Date()+"\nCouldn't generate a drone bee\n");
    }
}
