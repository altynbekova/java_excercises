package com.altynbekova.bees_simulator.service;

import com.altynbekova.bees_simulator.entity.WorkerBee;

import java.util.Date;
import java.util.Random;

public class WorkersHabitat extends Habitat {
    private int workersBirthPeriodicity;
    private double workersBirthProbability;

    public WorkersHabitat() {
    }

    public WorkersHabitat(int workersBirthPeriodicity, double workersBirthProbability) {
        this.workersBirthPeriodicity = workersBirthPeriodicity;
        this.workersBirthProbability = workersBirthProbability;
    }

    @Override
    public WorkerBee generateNewBee() {
        return new WorkerBee(workersBirthPeriodicity, workersBirthProbability);
    }

    @Override
    public void run() {
        Random random = new Random();
        double randomValue = random.nextDouble();
        if(randomValue<= workersBirthProbability) {
            System.out.println(new Date());
            WorkerBee bee = generateNewBee();
            System.out.println(bee + " generated {birthProbability=" + bee.getBirthProbability() +
                    ", randomValue=" + randomValue+ "}\n");

            bees.add(bee);
            workersAmount++;
        }
        else
            System.out.println(new Date()+"\nCouldn't generate a worker bee with randomValue="+randomValue+"\n");
    }
}
