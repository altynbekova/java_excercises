package com.altynbekova.bees_simulator.service;

import com.altynbekova.bees_simulator.entity.Bee;

public interface IBehaviour<T extends Bee> {
   T generateNewBee();
}
