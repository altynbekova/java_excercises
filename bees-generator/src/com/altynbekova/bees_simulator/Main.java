package com.altynbekova.bees_simulator;

import com.altynbekova.bees_simulator.service.DronesHabitat;
import com.altynbekova.bees_simulator.service.Habitat;
import com.altynbekova.bees_simulator.service.WorkersHabitat;

import java.util.Timer;

public class Main {
    private static final double DRONES_BIRTH_COEFFICIENT = 0.40;
    private static final int DRONES_BIRTH_PERIODICITY = 5;
    private static final double WORKERS_BIRTH_PROBABILITY=0.6;
    private static final int WORKERS_BIRTH_PERIODICITY=4;
    private static final int SIMULATION_TIME_SEC=25;

    public static void main(String[] args) {
        Habitat dronesHabitat = new DronesHabitat(DRONES_BIRTH_PERIODICITY,DRONES_BIRTH_COEFFICIENT);
        Habitat workersHabitat = new WorkersHabitat(WORKERS_BIRTH_PERIODICITY,WORKERS_BIRTH_PROBABILITY);
        Timer timer = new Timer(true);

        timer.scheduleAtFixedRate(dronesHabitat,0,DRONES_BIRTH_PERIODICITY*1000);
        timer.scheduleAtFixedRate(workersHabitat,0,WORKERS_BIRTH_PERIODICITY*1000);
        try {
            Thread.sleep(SIMULATION_TIME_SEC*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        timer.cancel();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("\nTotal amount of generated bees: "+Habitat.getBees().size());
        System.out.println("drone bees: "+dronesHabitat.getDronesAmount());
        System.out.println("worker bees: "+workersHabitat.getWorkersAmount());
        System.out.println("Simulation time: "+SIMULATION_TIME_SEC+" seconds");
    }
}
